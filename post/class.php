<?php

class Calc {

    public $_x, $_y, $_Op;

    public function __construct($x = 0, $y = 0, $Op = "") {
        $this->X = $x;
        $this->Y = $y;
        $this->Op = $Op;
    }

    public function _getX() {
        return $this->_x;
    }

    public function _getY() {
        return $this->_y;
    }
    
    public function _getOp() {
        return $this->_Op;
    }

    public function _setX($x) {
        if (!is_int($x)) {
            throw new InvalidArgumentException(__CLASS__ . '::$X doit être entier');
        } else {
            $x = intval($x);
            $this->_x = $x;
        }
    }

    public function _setY($y) {
        if (!is_int($y)) {
            throw new InvalidArgumentException(__CLASS__ . '::$Y doit être entier');
        } else {
            $y = intval($y);
            $this->_y = $Y;
        }
    }
    
    public function _setOp($Op) {
        if (!is_string($Op)) {
            throw new InvalidArgumentException(__CLASS__ . '::$oper doit être un string');
        } else {
            $this->_Op = $Op;
        }
    }

    public function operation($x, $y, $Op) {

        $result = "";

        switch ($Op) {
            case '+':
                $result = $x + $y;
                break;
            case 'x':
                $result = $x * $y;
                break;
            case '/':
                $result = $x  / $y;
                break;
            case '-':
                $result = $x  - $y;
                break;
        }

        return $result;
    }

}

if (isset($_POST['x']) && isset($_POST['y'])) {

    $x = (int) $_POST['x'];
    $y = (int) $_POST['y'];
    $Op = $_POST['select'];

    $calcul = new Calc($x, $y, $Op);
    
    $newX = $calcul->X;
    $newY = $calcul->Y;
    $newOp = $calcul->Op;
    
    $result = $calcul->operation($newX, $newY, $newOp);
    echo $result;

}